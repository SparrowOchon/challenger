# Challenger
##### Made primarily on an Android phone during my transit.

Programming Challenge generator.

## Install:
Download and install python3 and pip3. This depends on your OS.

* Install pre-reqs
`pip3 install -r requirements.txt`

## Usage:

* Get a new project:
`python3 challenge.py -n`

* Mark Project as Complete:
`python3 challenge.py -c`

* Change Difficulty:
`python3 challenge.py -d <new difficulty number>`

* Current Supported Difficulties:
1. Getting started
2. Junior level
3. Intermediate level
4. Senior level
5. Expert level

###### Note Expert level and Senior challenges arent that different its simply mastery of a domain that makes one more complicated then the other.


## Docker Image
A pre-setup ubuntu docker image has already been supplied with a few installed packages and a [vim](https://gitlab.com/SparrowOchon/neovim-setup) allowing instant challenge development.

* Docker image building
```
docker build -t challenger_image .
```
You can now setup your IDE's to use a remote docker config<br>

* If you need to edit the dev image (Installing new packages or working with cli)
```
docker run -ti challenger_image
```

## Contribution:
Have a programming project that you think would be good to implement ? Create an issue with the project description aswell as the difficulty level to have it reviewed.
