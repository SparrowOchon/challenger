#!/bin/python3
# ================================================================================================
#
# FILENAME :        challenge.py
#
# DESCRIPTION : Programming challenge selector. Select a project from storage.db based on
#                   defined difficulty stored in challenge-settings.yaml
#
# USAGE:
# 		challenge.py < -n | -c | -h | -d <New Challenge Level> >
#
#
#
# AUTHOR :   Network Silence        START DATE :    10 July 2019
#
#
# LICENSE :
#   Show don't Sell License Version 1
#   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
#
# 	Copyright (c) 2019, Network Silence
# 		All rights reserved.
#
#
#
#
# ===============================================================================================
import sqlite3
import sys
import getopt
import os
import yaml


class DbManager:
    """
    @brief: Database manager class. Abstract all db managing.
    """

    def __init__(self, db_name):
        self.db_conn = sqlite3.connect(db_name)

    def db_query(self, sql_string, sql_params):
        """
        @brief Launch a sql query based on a passed sql string.

        @param:
            sql_string {String} An Sql string to execute against the db

        @return {Dictionary} Dictionary return of the executed sql string.
        """
        try:
            db_cursor = self.db_conn.cursor()
            db_cursor.execute(sql_string, sql_params)
            if sql_string.split()[0].lower() != "select":
                self.db_conn.commit()
            return db_cursor.fetchone()
        except sqlite3.ProgrammingError:
            raise Exception("Failed to process sql statement")

    def db_close(self):
        """
        @brief Close the db connection
        """
        self.db_conn.close()


# =================================================================================================
class FileManager:
    """
    @brief Static yaml file manager class.
    """

    @staticmethod
    def file_in_dir(file_name, dir_name="."):
        """
        @brief Check if file exists in dir

        @params:
            file_name {String} Name of file who's existance we want to check.
            dir_name {String} Directory in which to check for file_name
        """
        return os.path.isfile(f"{dir_name}/{file_name}")

    @staticmethod
    def read_yaml(file_name):
        """
        @brief Read the content of a yaml file

        @params:
            file_name {String} File to read content from.

        @return {Dictionary} Dictionary of Yaml fields read from file
        """
        try:
            with open(file_name, "r") as opened_yaml_file:
                return yaml.safe_load(opened_yaml_file)
        except (yaml.YAMLError, FileNotFoundError, IOError, EOFError):
            raise Exception(f"Unable to read file: {file_name}")

    @staticmethod
    def write_yaml(file_name, input_dict):
        """
        @brief Write content from a dictionary to a yaml file.

        @params:
            file_name {String} Name of file we wish to write to
            input_dict {Dictionary} Dictionary which we wish to write to yaml file.
        """
        try:
            with open(file_name, "w", encoding="utf8") as outfile:
                yaml.dump(
                    input_dict, outfile, default_flow_style=False, allow_unicode=True
                )
        except (yaml.YAMLError, FileNotFoundError, IOError):
            raise Exception(f"Unable to write to file: {file_name}")


# ================================================================================================
def main(argv):
    """
    @brief  Start of program
    """
    db_name = "assets/storage.db"
    config_file_path = "assets/challenge-settings.yaml"
    challenge_id = None
    challenge_difficulty = 1
    db_manager = DbManager(db_name)

    if verify_files(db_name, config_file_path):
        challenge_difficulty, challenge_id = read_config(config_file_path)
    new_challenge = parse_input_parameters(
        argv, db_manager, challenge_difficulty, challenge_id
    )
    update_config(config_file_path, new_challenge)
    db_manager.db_close()


def new_project(difficulty, db_manager):
    """
    @brief: Select a new project from db.

    @params:
            difficulty {integer} Difficulty of challenge we are searching for.
            db_manager {Object} DbManager Object to manage db queries.

    @return {Array} Random incomplete challenge from db matching difficulty
    """
    MAX_DIFFICULTY = 5
    MIN_DIFFICULTY = 1
    try:
        if int(difficulty) > MAX_DIFFICULTY or int(difficulty) < MIN_DIFFICULTY:
            print_error(
                "Invalid Difficulty level chosen: Current MIN={MIN_DIFFICULTY}, \
                    Current MAX={MAX_DIFFICULTY}"
            )
        sql_params = (difficulty,)
        sql_query = f"Select id, description, difficulty from challengelist where is_completed=0 \
            and difficulty=? ORDER BY RANDOM() Limit 1"
        response_array = db_manager.db_query(sql_query, sql_params)
        print(response_array[1])  # 0->ID, 1->Description, 2->Difficulty
        return response_array
    except Exception as err:
        print(err)
        print_error(f"No Challenge found of difficulty:{difficulty}")


def update_project(completed_id, db_manager):
    """
    @brief: Update Db entry with passed id to completed

    @params:
        completed_id {Integer} Database ID of challenge completed
        db_manager {Object} DbManager Object to manage db queries.
    """
    try:
        sql_params = (completed_id,)
        sql_query = f"Update challengelist set is_completed=1 where id=?"
        db_manager.db_query(sql_query, sql_params)
    except Exception as err:
        print_error(f"Unable to complete project of id:{completed_id}")


def verify_files(db_name, settings_file_path):
    """
    @brief Verify the presence of files in the present working directory

    @params:
        db_name {String} Name of the database file we will be accessing
        settings_file_path {String} Yaml settings file which contains scripts settings.

    @return {Bool} Return a Boolean representing the presence of the files in the
            current directory.
    """
    try:
        return FileManager.file_in_dir(db_name) and FileManager.file_in_dir(
            settings_file_path
        )
    except Exception as err:
        print_error(err)


def read_config(settings_file_path):
    """
    @brief Read yaml settings file

    @params:
        settings_file_path {String} Yaml settings file in present working directory.

    @return:
        {Tuple}:
            {String} The difficulty rating set in the settings file.
            {String} The current challenge id being worked on.
    """
    try:
        settings_data = FileManager.read_yaml(settings_file_path)
        return settings_data["difficulty"], settings_data["challenge_id"]
    except Exception:
        print_error(f"Unable to read file: {settings_file_path}")


def update_config(settings_file_path, new_challenge):
    """
    @brief Update the Yaml setting file with the new challenge
        info being worked on.

    @params:
        settings_file_path {String} Settings file which we want to write to
        new_challenge {Array} Array of data which we want to print into settings file.
    """
    try:
        challenge_dict = {
            "challenge_id": new_challenge[0],
            "description": new_challenge[1],
            "difficulty": new_challenge[2],
        }
        FileManager.write_yaml(settings_file_path, challenge_dict)
    except Exception:
        print_error(f"Unable to write to file: {settings_file_path}")


def print_error(error_message):
    """
    @brief Print error message and kill program.

    @params:
        error_message {String} Error message to print for the user
    """
    print(f"Error Occured: {error_message}")
    sys.exit(1)


def parse_input_parameters(argv, db_manager, challenge_difficulty=1, challenge_id=None):
    """
    @brief Manage passed parameters creating and updating database entries based on the
            passed parameters.

    @params:
        argv {Array} Array of all user passed parameters
        db_manager {Object} DbManager object to manage db queries
        challenge_difficulty {Integer} The current set difficulty setting
        challenge_id {Integer} Database ID of the current challenge being worked on.

    @return {Array}
    """
    try:
        if len(argv) == 0:
            print_error("No parameters passed")
        opts, _ = getopt.getopt(
            argv, "hncd:", ["--help", "--new", "--complete", "--difficulty="]
        )
    except getopt.GetoptError:
        print(f"{sys.argv[0]} <option>")
        print("Invalid option, please use -h to view available commands")
        sys.exit(1)
    for opt, arg in opts:
        if opt == ("-h", "--help"):
            print("Options List")
            print("\t\t-n, --new : New problem")
            print("\t\t-c, --complete : complete problem of defined id")
            print(
                "\t\t-d, --difficulty <level> : Change difficulty level to desired level"
            )
            print("\t\t\t Levels")
            print("\t\t\t 1:Just Getting Started,")
            print("\t\t\t 2:Junior Level Developer,")
            print("\t\t\t 3:Intermediate Level Developer,")
            print("\t\t\t 4:Senior Level developer,")
            print("\t\t\t 5:Expert Level developer")
            sys.exit()
        elif opt in ("-c", "--complete"):
            update_project(challenge_id, db_manager)
            # Add validation on proper complete later
            print(f"Challenge {challenge_id} completed successfully")

        elif opt in ("-d", "--difficulty"):
            challenge_difficulty = arg

        elif opt in ("-n", "--new"):
            pass

        return new_project(challenge_difficulty, db_manager)


if __name__ == "__main__":
    main(sys.argv[1:])
